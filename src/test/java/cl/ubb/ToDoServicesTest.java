package cl.ubb;

import static org.junit.Assert.*;

import static org.mockito.Mockito.when;

import java.util.ArrayList;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import antlr.collections.List;
import cl.ubb.dao.ToDoDao;
import cl.ubb.model.ToDo;
import cl.ubb.service.ToDoServices;

@RunWith(MockitoJUnitRunner.class)
public class ToDoServicesTest {
	
	@Mock
	private ToDoDao toDoDao;
	
	@InjectMocks
	private ToDoServices toDoServices;
/*
	@Before
	public void setUp() throws Exception {
	}
*/
	@Test
	public void guardarToDoYRetornarlo() {
		
		ToDo tarea=new ToDo();
		ToDo tareacreada=new ToDo();
		
		when(toDoDao.save(tarea)).thenReturn(tarea);
		tareacreada=toDoServices.crearToDo(tarea);
		
		Assert.assertNotNull(tareacreada);
		Assert.assertEquals(tarea, tareacreada);
		
		
		
	}
	
	@Test
	public void buscarToDoPorId() {
		
		ToDo tarea=new ToDo();
		tarea.setId(1L);
		
		ToDo tarearetornada=new ToDo();
		
		when(toDoDao.findOne(1L)).thenReturn(tarea);
		tarearetornada=toDoServices.obtenerToDo(tarea.getId());
		
		Assert.assertNotNull(tarearetornada);
		Assert.assertEquals(tarea, tarearetornada);
		
		
		
	}
	
	@Test
	public void buscarToDoPorCategoria() {
		
		ToDo tarea=new ToDo();
		tarea.setCategoria("cualquiera");
		
		ToDo tarearetornada=new ToDo();
		
		when(toDoDao.findByCategoria("cualquiera")).thenReturn(tarea);
		tarearetornada=toDoServices.obtenerToDo(tarea.getCategoria());
		
		Assert.assertNotNull(tarearetornada);
		Assert.assertEquals(tarea, tarearetornada);
		
		
		
	}
	
	@Test
	public void buscarToDoPorEstado() {
		
		ToDo tarea=new ToDo();
		tarea.setEstado(true);
		ToDo tarearetornada=new ToDo();
		
		when(toDoDao.findByEstado(true)).thenReturn(tarea);
		tarearetornada=toDoServices.obtenerToDo(tarea.isEstado());
		
		Assert.assertNotNull(tarearetornada);
		Assert.assertEquals(tarea, tarearetornada);
		
		
		
	}
	
	@Test
	public void obtenerTodosLosToDoEnUnaLista() {
		
		ArrayList<ToDo> tareas = new ArrayList<ToDo>();
		ArrayList<ToDo> tareasretornadas = new ArrayList<ToDo>();
		
		
		when(toDoDao.findAll()).thenReturn(tareas);
		tareasretornadas = toDoServices.obtenerTodos();
		
		
		Assert.assertNotNull(tareasretornadas);
		Assert.assertEquals(tareas, tareasretornadas);
		
		
		
	}
	
	@Test
	public void eliminarToDo() {
		
		ToDo tarea=new ToDo();
		tarea.setId(1L);
		
		ToDo tarearetornada=new ToDo();
		
		when(toDoDao.findOne(1L)).thenReturn(tarea);
		tarearetornada=toDoServices.eliminarToDo(tarea.getId());
			
		
		Assert.assertNotNull(tarearetornada);
		Assert.assertEquals(tarea, tarearetornada);
		
		
		
	}
	
	

}
