package cl.ubb.service;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;

import cl.ubb.dao.ToDoDao;
import cl.ubb.model.ToDo;

public class ToDoServices {
	
	private final ToDoDao toDoDao;
	
	@Autowired
	public ToDoServices(ToDoDao toDoDao){
		
		this.toDoDao=toDoDao;
		
	}

	public ToDo crearToDo(ToDo tarea) {
		// TODO Auto-generated method stub
		return toDoDao.save(tarea);
	}

	public ToDo obtenerToDo(long id) {
		// TODO Auto-generated method stub
		
		ToDo tarea=new ToDo();
		tarea=toDoDao.findOne(id);
		return tarea;
	}

	public ToDo obtenerToDo(String categoria) {
		// TODO Auto-generated method stub
		ToDo tarea=new ToDo();
		tarea=toDoDao.findByCategoria(categoria);
		return tarea;
	}

	public ToDo obtenerToDo(boolean estado) {
		// TODO Auto-generated method stub
		ToDo tarea=new ToDo();
		tarea=toDoDao.findByEstado(estado);
		return tarea;
	}

	public ArrayList<ToDo> obtenerTodos() {
		// TODO Auto-generated method stub
		ArrayList<ToDo> tareas=new ArrayList<ToDo>();
		
		tareas=(ArrayList<ToDo>) toDoDao.findAll();
		
		return tareas;
	}

	public ToDo eliminarToDo(long id) {
		// TODO Auto-generated method stub
		ToDo tarea=new ToDo();
		tarea=toDoDao.findOne(id);
		toDoDao.delete(tarea);
		return tarea;
	}

}
